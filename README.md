# week5 - 9-10 December 2021

Day | Lesson
---|---
9 December | [Text wrangling](./scripts/Text_Wrangling.ipynb)
9 December | [Stemming, Lemmatization, Stopwords, N-grams](./scripts/Stemming_Lemmatization_Stopword_Case_Folding_N_grams_HTML_tags.ipynb)
9 December | [Text Feature Selection](./scripts/text_feature_selection_partI.ipynb)
10 December | [Solution Exercise - Lesson 12](./exercises/Lesson12_exercise.ipynb)
10 December | [Text Feature Selection](./scripts/text_feature_selection_partII.ipynb) 
10 December | [Clustering text feagure with Hierarchical clustering](./scripts/clustering_text_feature.ipynb)
10 December | [Text classification example](./scripts/text_classification.ipynb)

Please have a look at the following notebooks:
* [Bag of words](./scripts/bag_of_words.ipynb)
* [TF-IDF](./scripts/tfidfvectorizer_for_text_representation.ipynb)
* implementing_cbow_model.ipynb	by hand with keras
